/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_canonique.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/14 09:45:42 by jle-quel          #+#    #+#             */
/*   Updated: 2017/09/14 19:34:39 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

/*
******************* PRIVATE ****************************************************
*/

static inline int	ft_errno(int error)
{
	if (error == NO_TMS)
		ft_putendl_fd("\e[38;5;1m21sh\x1b[0m: Informations couldn't be found.", 2);
	else if (error == NO_SET)
		ft_putstr_fd("\e[38;5;1m21sh\x1b[0m: Couldn't change value of struct termios.", 2);
	return (-1);
}

/*
******************* PUBLIC *****************************************************
*/

int					ft_canonique(void)
{
	struct termios	term;

	if (tcgetattr(0, &term) == -1)
		return (ft_errno(NO_TMS));
	term.c_lflag |= (ICANON | ECHO);
	if (tcsetattr(0, TCSANOW, &term) == 1)
		return (ft_errno(NO_SET));
	return (0);
}
