/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_place_cursor.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/14 10:41:57 by jle-quel          #+#    #+#             */
/*   Updated: 2017/09/14 19:34:12 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

/*
******************* PRIVATE ****************************************************
*/

/*
******************* PUBLIC *****************************************************
*/

int		ft_place_cursor(int width, int height, int fd)
{
	char	*cursor_move;

	if (!(cursor_move = tgetstr("cm", NULL)))
	{
		ft_putendl_fd("\e[38;5;1m21sh\x1b[0m: Termcap for cursor couldn't be found", 2);
		return (-1);
	}
	ft_putstr_fd(tgoto(cursor_move, width, height), fd);
	return (0);
}
