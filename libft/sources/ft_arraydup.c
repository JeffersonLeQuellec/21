/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arraydup.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/17 14:06:01 by jle-quel          #+#    #+#             */
/*   Updated: 2017/07/17 14:06:14 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	**ft_arraydup(char **array)
{
	size_t	index;
	size_t	len;
	char	**ret;

	index = 0;
	len = ft_arraylen(array);
	CHK_CC((ret = (char**)malloc(sizeof(char*) * len + 1)));
	while (index < len)
	{
		ret[index] = ft_strdup(array[index]);
		index++;
	}
	ret[index] = NULL;
	return (ret);
}
