/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pimplen.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/13 13:02:25 by jle-quel          #+#    #+#             */
/*   Updated: 2017/07/08 14:08:04 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

size_t		ft_pimplen(const char *str, int n)
{
	int		index;

	index = 0;
	while (str[n] != '\0')
		index++;
	return (index);
}
