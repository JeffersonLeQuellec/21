/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_treeinsert.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/19 10:44:41 by jle-quel          #+#    #+#             */
/*   Updated: 2017/08/19 11:05:00 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

static t_tree	*ft_treenew(char *name)
{
	t_tree	*new;

	CHK_CC((new = (t_tree*)malloc(sizeof(t_tree))));
	CHK_CC((new->content = ft_strdup(name)));
	new->left = NULL;
	new->right = NULL;
	return (new);
}

t_tree			 *ft_treeinsert(t_tree *node, char *name)
{
	if (node == NULL)
		return (ft_treenew(name));
	else if (ft_strcmp(name, node->content) <= 0)
		node->left = ft_treeinsert(node->left, name);
	else if (ft_strcmp(name, node->content) > 0)
		node->right = ft_treeinsert(node->right, name);
	return (node);
}
