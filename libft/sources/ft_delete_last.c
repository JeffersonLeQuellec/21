/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_delete_last.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 14:26:58 by jle-quel          #+#    #+#             */
/*   Updated: 2017/08/14 14:29:16 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

void	ft_delete_last(t_list **node)
{
	t_list	*temp;
	t_list	*old;

	temp = *node;
	while (temp->next)
	{
		old = temp;
		temp = temp->next;
	}
	old->next = NULL;
	ft_delete_node(&temp);
	temp = NULL;
}
