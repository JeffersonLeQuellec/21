/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_freetree.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/19 10:54:33 by jle-quel          #+#    #+#             */
/*   Updated: 2017/08/19 10:54:52 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

void		ft_freetree(t_tree *node)
{
	CHK_CV(node);
	ft_treeclr(node->left);
	ft_treeclr(node->right);
	free(node->name);
	if (node->path)
		free(node->path);
	node->name = NULL;
	node->path = NULL;
	free(node);
	node = NULL;
}
