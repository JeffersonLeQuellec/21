/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_noncanonique.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/14 09:48:02 by jle-quel          #+#    #+#             */
/*   Updated: 2017/09/14 19:34:20 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

/*
******************* PRIVATE ****************************************************
*/

static inline int	ft_errno(int error)
{
	if (error == NO_TMS)
		ft_putendl_fd("\e[38;5;1m21sh\x1b[0m: Informations couldn't be found.", 2);
	else if (error == NO_SET)
		ft_putendl_fd("\e[38;5;1m21sh\x1b[0m: Couldn't change value of struct termios.", 2);
	return (-1);
}

/*
******************* PUBLIC *****************************************************
*/

int					ft_noncanonique(void)
{
	struct termios	term;

	if (tcgetattr(0, &term) == -1)
		return (ft_errno(NO_TMS));
	term.c_lflag &= ~(ICANON | ECHO);
	term.c_cc[VMIN] = 1;
	term.c_cc[VTIME] = 0;
	if (tcsetattr(0, TCSADRAIN, &term) == 1)
		return (ft_errno(NO_SET));
	return (0);
}
