/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/10 12:34:45 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/03 19:45:07 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strcat(char *s1, const char *s2)
{
	int		index;
	int		len;

	index = 0;
	len = 0;
    if (s1)
    {
	    while (s1[index] != '\0')
		    index++;
    }
    if (s2)
    {
	    while (s2[len] != '\0')
		    s1[index++] = s2[len++];
	}
	s1[index] = '\0';
	return (s1);
}
