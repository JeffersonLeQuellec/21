/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_movecursor_up.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/03 18:34:19 by jle-quel          #+#    #+#             */
/*   Updated: 2017/08/03 18:34:32 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libft.h"

int		ft_movecursor_up(void)
{
	char	*cursor;

	if (!(cursor = tgetstr("up", NULL)))
		return (ft_errno(NO_CUR));
	ft_putstr_fd(cursor, 0);
	return (0);
}
