/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jefferso <jefferso@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/18 14:58:09 by jefferso          #+#    #+#             */
/*   Updated: 2017/08/18 14:59:53 by jefferson        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

t_list	*ft_lstdup(t_list *node)
{
	t_list	*new;
	t_list	*temp;

	new = NULL;
	while (node)
	{
		if (new == NULL)
			new = ft_lstnew(node->content, 0);
		else
		{
			temp = ft_lstnew(node->content, 0);
			ft_lstaddnext(&new, &temp);
		}
		node = node->next;
	}
	return (new);
}
