/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/10 15:21:22 by jle-quel          #+#    #+#             */
/*   Updated: 2017/07/08 14:10:01 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strrchr(const char *s, int c)
{
	int		index;

	index = ft_strlen((char*)s);
	while (index > 0 && s[index] != (char)c)
		index--;
	if (s[index] == (char)c)
		return ((char*)s + index);
	return (NULL);
}
