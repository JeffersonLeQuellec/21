/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrlen.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/02 14:06:49 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/02 14:09:07 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

/*
******************* PRIVATE ****************************************************
*/

/*
******************* PUBLIC *****************************************************
*/
unsigned int		ft_strrlen(char *str, char c, unsigned int index)
{
	unsigned int	length;

	length = 0;
	if (str)
	{
		while (str[index])
		{
			if (str[index] == c)
				break ;
			index++;
			length++;
		}
	}
	return (length);
}
