/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_array_to_char.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/06 15:19:37 by jle-quel          #+#    #+#             */
/*   Updated: 2017/09/06 16:23:52 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

/*
******************* PRIVATE ****************************************************
*/

/*
******************* PUBLIC *****************************************************
*/

char				*ft_array_to_char(char ***array)
{
	register unsigned int	index;
	char					**temp;
	char					*ret;

	index = 1;
	temp = *array;
	ret = ft_memalloc(ft_length_of_array(temp) + ft_arraylen(temp));
	ft_strcpy(ret, temp[0]);

	while (temp[index])
	{
		ft_strcat(ret, " ");
		ft_strcat(ret, temp[index++]);
	}
	ft_arraydel(array);
	return (ret);
}
