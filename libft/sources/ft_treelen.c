/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_treelen.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/19 10:54:05 by jle-quel          #+#    #+#             */
/*   Updated: 2017/08/19 10:59:20 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

size_t	ft_treelen(t_tree *node)
{
	if (!node)
		return (0);
	else
		return (ft_treelen(node->left) + 1 + ft_treelen(node->right));
}
