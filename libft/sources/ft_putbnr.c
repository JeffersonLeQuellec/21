#include "../includes/libft.h"

void	ft_putbnr(int nb)
{
	int		index;

	index = 4096;
	while (index > 0)
	{
		if (nb < index)
			ft_putnbr(0);
		else
		{
			ft_putnbr(1);
			nb -= 2;
		}
		index /= 2;
	}
}
