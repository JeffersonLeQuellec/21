/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printarray_fd.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jefferso <jefferso@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/26 14:48:43 by jefferso          #+#    #+#             */
/*   Updated: 2017/08/27 14:19:20 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"


void	ft_printarray_fd(char **array, int fd)
{
	size_t	index;

	index = 0;
	if (array)
	{
		while (array[index])
		{
			ft_putstr_fd(array[index], fd);
			ft_putchar(' ');
			index++;
		}
	}
}
