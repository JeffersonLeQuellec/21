/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_remove_char.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/07 10:18:44 by jle-quel          #+#    #+#             */
/*   Updated: 2017/09/20 17:53:10 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

/*
******************* PRIVATE ****************************************************
*/

/*
******************* PUBLIC *****************************************************
*/

void				ft_remove_char(char **str, char c)
{
	register unsigned int	index;
	register unsigned int	i;
	char					*temp;
	char					*new;

	index = 0;
	i = 0;
	temp = *str;
	new = ft_memalloc(ft_strlen(temp) + 1);
	while (temp[index])
	{
		if (temp[index] != c)
		{
			new[i] = temp[index];
			i++;
		}
		index++;
	}
	ft_strdel(str);
	*str = new;
}
