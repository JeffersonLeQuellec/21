/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_inittermcap.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/14 09:41:31 by jle-quel          #+#    #+#             */
/*   Updated: 2017/09/14 19:34:30 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

/*
******************* PRIVATE ****************************************************
*/

static inline int	ft_errno(int error)
{
	if (error == NO_VAR)
		ft_putendl_fd("\e[38;5;1m21sh\x1b[0m: Variable $TERM is not defined.", 2);
	else if (error == NO_ENTRY)
		ft_putendl_fd("\e[38;5;1m21sh\x1b[0m: No such entry.", 2);
	else if (error == NO_DB)
		ft_putendl_fd("\e[38;5;1m21sh\x1b[0m: Terminfo database couldn't be found.", 2);
	return (-1);
}

/*
******************* PUBLIC *****************************************************
*/

int					ft_inittermcap(void)
{
	int				result;
	char			*terminal_type;

	if (!(terminal_type = getenv("TERM")))
		return (ft_errno(NO_VAR));
	if ((result = tgetent(NULL, terminal_type)) == 0)
		return (ft_errno(NO_ENTRY));
	if (result == -1)
		return (ft_errno(NO_DB));
	return (0);
}
