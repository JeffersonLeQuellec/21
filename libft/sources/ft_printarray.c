/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printarray.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jefferso <jefferso@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/05 12:49:13 by jefferso          #+#    #+#             */
/*   Updated: 2017/08/21 13:28:05 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

void	ft_printarray(char **array)
{
	size_t	index;

	index = 0;
	if (array)
	{
		while (array[index])
		{
			ft_putendl(array[index]);
			index++;
		}
	}
}
