/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pointers_before.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/07 10:47:27 by jle-quel          #+#    #+#             */
/*   Updated: 2017/09/10 10:21:10 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

/*
******************* PRIVATE ****************************************************
*/

/*
******************* PUBLIC *****************************************************
*/

void				ft_pointers_before(t_list **node, unsigned int stop)
{
	register unsigned int	index;

	index = 0;
	if (*node)
	{
		while ((*node)->before)
		{
			if (index == stop)
				break ;
			*node = (*node)->before;
			index++;
		}
	}
}
