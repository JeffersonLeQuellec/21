/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putlst.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jefferso <jefferso@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/01 18:35:52 by jefferso          #+#    #+#             */
/*   Updated: 2017/09/10 10:18:36 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

void	ft_putlst(t_list *node, int flag, char direction)
{
	t_list	*temp;

	temp = node;
	while (temp)
	{
		flag == 1 ? ft_putendl(temp->content) : ft_putstr(temp->content);
		temp = direction == 'n' ? temp->next : temp->before;
	}
}
