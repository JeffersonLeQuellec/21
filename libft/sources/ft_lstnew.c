/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/20 11:54:14 by jle-quel          #+#    #+#             */
/*   Updated: 2017/07/24 18:38:00 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

t_list	*ft_lstnew(const char *content, size_t content_size)
{
	t_list	*node;

	if (!(node = (t_list*)malloc(sizeof(t_list))))
		return (NULL);
	if (!content)
	{
		node->content = NULL;
		node->flag = 0;
	}
	else
	{
		if (!(node->content = ft_strsub(content, 0, ft_strlen(content))))
		{
			free(node);
			return (NULL);
		}
		node->flag = content_size;
	}
	node->next = NULL;
	node->before = NULL;
	return (node);
}
