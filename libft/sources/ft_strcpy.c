/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/10 11:27:28 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/03 19:45:56 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strcpy(char *dst, const char *src)
{
	int				index;

	index = 0;
    if (dst && src)
    {
	    while (src[index] != '\0')
	    {
		    dst[index] = src[index];
		    index++;
	    }
	    dst[index] = '\0';
    }
	return (dst);
}
