/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printtreeOrder.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/19 10:58:15 by jle-quel          #+#    #+#             */
/*   Updated: 2017/08/19 11:05:29 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

void	ft_printtreeOrder(t_tree *node)
{
	CHK_CV(node);
	ft_printtreeOrder(node->left);
	ft_putendl(node->content);
	ft_printtreeOrder(node->right);
}
