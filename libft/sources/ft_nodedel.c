/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_delete_node.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/20 14:49:52 by jle-quel          #+#    #+#             */
/*   Updated: 2017/09/21 12:13:21 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

void		ft_nodedel(t_list **node)
{
    t_list  *temp;

    temp = *node;
    if (temp)
    {
	    ft_strdel(&temp->content);
	    free(temp);
        *node = NULL;
    }
}
