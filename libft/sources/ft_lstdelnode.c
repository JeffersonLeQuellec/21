/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdelnode.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/03 10:52:45 by jle-quel          #+#    #+#             */
/*   Updated: 2017/07/08 14:07:13 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

t_list	*ft_lstdelnode(t_list *node)
{
	t_list	*temp;
	t_list	*rm;

	temp = node;
	rm = node;
	temp = temp->next;
	free(rm);
	return (temp);
}
