/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/20 17:20:51 by jle-quel          #+#    #+#             */
/*   Updated: 2017/08/14 13:33:27 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

void	ft_lstaddhead(t_list **node, char *command)
{
	t_list	*new;

	new = ft_lstnew(command, 0);
	new->next = *node;
	(*node)->before = new;
	*node = new;
}
