#include "../includes/libft.h"

char    **ft_arraysub(char **array, unsigned int start, unsigned int length)
{
    register unsigned int   index;
    char                    **ret;

    index = 0;
    ret = NULL;
    if (array)
    {   ret = (char**)malloc(sizeof(char*) * length + 1);
        if (ret == NULL)
        {
            ft_putendl_fd("No virtual memory left", 2);
            exit(-1);
        }
        while (length--)
            ret[index++] = ft_strdup(array[start++]);
        ret[index] = NULL;
    }
    return (ret);
}
