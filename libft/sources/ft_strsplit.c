/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/14 16:55:51 by jle-quel          #+#    #+#             */
/*   Updated: 2017/09/14 09:36:33 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

static int		count_letters(const char *s, char c)
{
	int		count;

	count = 0;
	while (*s && *s != c)
	{
		s++;
		count++;
	}
	return (count);
}

char			**ft_strsplit(const char *s, char c)
{
	int		i;
	int		words;
	char	**array;

	if (!s || !c)
		return (0);
	i = 0;
	words = ft_countwords(s, c);
	array = (char**)malloc(sizeof(array) * words + 1);
	if (!array)
		return (0);
	while (words--)
	{
		while (*s && *s == c)
			s++;
		array[i] = ft_strsub(s, 0, count_letters(s, c));
		if (!array[i])
			return (0);
		s = s + count_letters(s, c);
		i++;
	}
	array[i] = 0;
	return (array);
}
