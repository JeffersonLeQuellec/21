/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcap.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/14 09:30:31 by jle-quel          #+#    #+#             */
/*   Updated: 2017/09/14 19:34:05 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

/*
******************* PRIVATE ****************************************************
*/

/*
******************* PUBLIC *****************************************************
*/

int					ft_termcap(char *str, int fd)
{
	char	*termcap;

	if (!(termcap = tgetstr(str, NULL)))
	{
		ft_putendl_fd("\e[38;5;1m21sh\x1b[0m: Termcap for cursor couldn't be found", 2);
		return (-1);
	}
	ft_putstr_fd(termcap, fd);
	return (0);
}
