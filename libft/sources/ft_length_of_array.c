/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_length_of_array.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/06 15:20:51 by jle-quel          #+#    #+#             */
/*   Updated: 2017/09/06 16:22:35 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

/*
******************* PRIVATE ****************************************************
*/

/*
******************* PUBLIC *****************************************************
*/

unsigned int		ft_length_of_array(char **argv)
{
	register unsigned int	index;
	unsigned int			length;

	index = 0;
	length = 0;

	while (argv[index])
	{
		length += ft_strlen(argv[index]);
		index++;
	}
	return (length);
}
