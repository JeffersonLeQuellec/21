/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pointers_next.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/07 10:46:59 by jle-quel          #+#    #+#             */
/*   Updated: 2017/09/10 10:20:48 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

/*
******************* PRIVATE ****************************************************
*/

/*
******************* PUBLIC *****************************************************
*/

void				ft_pointers_next(t_list **node, unsigned int stop)
{
	register unsigned int	index;
	t_list					*temp;

	index = 0;
	temp = *node;
	if (*node)
	{
		while ((*node)->next)
		{
			if (index == stop)
				break ;
			*node = (*node)->next;
			index++;
		}
	}
}
