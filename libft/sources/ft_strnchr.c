/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/02 17:35:43 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/02 17:36:21 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

/*
******************* PRIVATE ****************************************************
*/

/*
******************* PUBLIC *****************************************************
*/

char				*ft_strnchr(const char *s, int c, unsigned int index)
{
	while (s[index] != '\0')
	{
		if (s[index] == (char)c)
			return ((char*)s + index);
		index++;
	}
	if (s[index] == (char)c)
		return ((char*)s + index);
	return (NULL);
}
