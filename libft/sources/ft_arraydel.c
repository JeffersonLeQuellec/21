/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arraydel.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/28 18:45:13 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/02 16:16:17 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

void	ft_arraydel(char ***array)
{
	size_t	index;
	size_t	len;
	char	**tmp;

	CHK_CV(*array);
	index = 0;
	len = ft_arraylen(*array);
	tmp = *array;
	while (index < len)
	{
		free(tmp[index]);
		tmp[index] = NULL;
		index++;
	}
	free(tmp);
	tmp = NULL;
}
