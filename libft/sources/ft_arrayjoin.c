/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arrayjoin.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/25 12:32:28 by jle-quel          #+#    #+#             */
/*   Updated: 2017/09/25 12:40:50 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

/*
******************* PRIVATE ****************************************************
*/

/*
******************* PUBLIC *****************************************************
*/

char				**ft_arrayjoin(char **a1, char **a2)
{
    register unsigned int   index;
    register unsigned int   i;
    char                    **new;

    new = NULL;
    if (!a1 && a2)
        return (ft_arraydup(a2));
    if (!a2 && a1)
        return (ft_arraydup(a1));
    if (!a1 && !a2)
        return (NULL);
    index = 0;
    i = 0;
    new = (char**)malloc(sizeof(char*) * (ft_arraylen(a1)+ft_arraylen(a2) + 1));
    while (a1[i])
        new[index++] = ft_strdup(a1[i++]);
    i = 0;
    while (a2[i])
        new[index++] = ft_strdup(a2[i++]);
    new[index] = NULL;
    return (new);
}
