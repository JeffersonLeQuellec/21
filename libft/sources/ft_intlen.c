/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_intlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/14 16:44:00 by jle-quel          #+#    #+#             */
/*   Updated: 2017/09/23 15:21:40 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

unsigned int    ft_intlen(unsigned int number)
{
    unsigned int    length;

    length = 0;
    while (number > 9)
    {
        length++;
        number /= 10;
    }
    return (length);
}
