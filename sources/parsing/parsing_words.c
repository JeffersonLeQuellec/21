/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing_words.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/02 16:07:23 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/02 16:14:02 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh.h"

/*
******************* PRIVATE ****************************************************
*/

char				chk_parsing(char *str)
{
	register unsigned int   index;
    unsigned int            flag;

    index = 0;
    flag = 0;
    while (str[index])
    {
        if (str[index] == ' ')
            flag++;
        index++;
    }
    return index == flag ? -1 : 0;
}

/*
******************* PUBLIC *****************************************************
*/

void				parsing_words(t_list **node)
{
	register unsigned int	index;
	char					err;
	t_list					*temp;

	index = 0;
	temp = *node;
	while (temp)
	{
		if (index % 2 == 0)
		{
			err = chk_parsing(temp->content);
			if (err == -1)
			{
				ft_errno(SYNTAX_ERR, temp->content);
                ft_lstdel(node);
                return ;
			}
		}
		index++;
		temp = temp->next;
	}
}
