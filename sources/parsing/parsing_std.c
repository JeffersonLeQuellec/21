/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing_std.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/23 16:38:27 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/02 16:26:09 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/sh.h"

/*
******************* PRIVATE ****************************************************
*/

static unsigned int	__attribute__((const))
					chk_occurence(char **str)
{
    unsigned int    occurence;
    unsigned int    length;

    occurence = 0;
    length = ft_strlen(*str) - 1;
    while ((*str)[length])
    {
        if (ft_isdigit((*str)[length]) == 1)
            occurence++;
        else
            break ;
        length--;
    }
    return (occurence);
}

static void         do_reconversion(char **old, char **temp,
					unsigned int occurence)
{
    unsigned int    length;
    char            *memory;
    char            *memory2;

    length = ft_strlen(*old) - occurence;
    memory = *old;
    *old = ft_strsub(*old, 0, length);

    memory2 = *temp;
    *temp = ft_strjoin(memory + length, *temp);
    ft_strdel(&memory);
    ft_strdel(&memory2);
}

/*
******************* PUBLIC *****************************************************
*/

void                parsing_std(t_list **node)
{
    t_list          *temp;
    t_list          *old;
    unsigned int    occurence;

    temp = *node;
    while (temp)
    {
        old = temp;
        temp = temp->next;
        if (temp && temp->content[0] == '>')
        {
            occurence = chk_occurence(&old->content);
            if (occurence > 0)
                do_reconversion(&old->content, &temp->content, occurence);
        }
    }
}
