/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing_redirection.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/25 12:56:01 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/02 18:57:38 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/sh.h"

/*
******************* PRIVATE ****************************************************
*/

static inline char	__attribute__((const))
					chk(char c)
{
    register unsigned short index;
    static const char       chk[4] = {'<', ';', '|', '\0'};

    index = 0;
    while (chk[index])
    {
        if (c == chk[index])
            return (1);
        index++;
    }
    return (-1);
}

static char         *get_rest(t_list *node)
{
    unsigned int    length;
    char            **temp;
    char            *rest;

    if (!node)
        return (NULL);
    length = ft_skip(node->content, 0, ' ');
    temp = ft_strsplit(node->content, ' ');
    rest = temp[1] ? node->content + (length + ft_strlen(temp[0])) : NULL;
    ft_arraydel(&temp);
    return (rest);
}

static void         remove_rest(t_list **node, char *rest)
{
    char    *new;

    if (!(*node))
        return ;
    new = ft_strsub((*node)->content, 0, ft_strlen((*node)->content) -
															ft_strlen(rest));
    ft_strdel(&(*node)->content);
    (*node)->content= new;
}

static void         add_argv(char **str, char **argv)
{
    char    *memory;

    memory = *str;
    *str = ft_strjoin(*str, *argv);
    ft_strdel(&memory);
    ft_strdel(argv);
}

/*
******************* PUBLIC *****************************************************
*/

void                parsing_redirection(t_list **node)
{
    t_list  *temp;
    char    *rest;
    char    *memory;
    char    *argv;

    temp = *node;
    rest = NULL;
    memory = NULL;
    argv = NULL;

    ft_pointers_next(&temp, ft_lstlen(temp));
    while (temp)
    {
        if (*temp->content == '>' || ft_isdigit(*temp->content) == 1)
        {
            rest = get_rest(temp->next);
            memory = argv;
            argv = ft_strrjoin(argv, rest);
            remove_rest(&temp->next, rest);
            ft_strdel(&memory);
        }
        else if (chk(*temp->content) == 1)
            add_argv(&temp->next->content, &argv);
        else if (!temp->before)
            add_argv(&temp->content, &argv);
        temp = temp->before;
    }
}
