/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing_operaters.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/03 11:01:35 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/02 16:20:27 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/sh.h"

/*
******************* PRIVATE ****************************************************
*/

static char			chk_parsing(char *str)
{
    register unsigned short index;
    static const char       *operaters[] =
	{";", "|", "<", "<<", ">", ">>", ">&", "<&", ">>&", NULL};

    index = 0;
    while (operaters[index])
    {
        if (ft_strcmp(str, operaters[index]) == 0)
            return (0);
        index++;
    }
    return (-1);
}

/*
******************* PUBLIC *****************************************************
*/

void				parsing_operaters(t_list **node)
{
	register unsigned int	index;
	char					err;
	t_list					*temp;

	index = 0;
	temp = *node;
	while (temp)
	{
		if (index % 2 == 1)
		{
			err = chk_parsing(temp->content);
			if (err == -1)
			{
				ft_errno(PARSE_ERR, temp->content);
                ft_lstdel(node);
                return ;
			}
		}
		index++;
		temp = temp->next;
	}
}
