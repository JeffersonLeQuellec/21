/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexer.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/30 17:37:38 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/02 16:05:39 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh.h"

/*
******************* PRIVATE ****************************************************
*/

void				chk_lexer(t_list **node)
{
    t_list  *temp;
    t_list  *memory;

    temp = *node;
    CHK_CV(temp);
    ft_pointers_next(&temp, ft_lstlen(temp));

    if (*temp->content == ';')
    {
        memory = temp;
        temp = temp->before;
        ft_nodedel(&memory);
        temp->next = NULL;
    }
    else if (chk_operaters(*temp->content) == 1)
    {
		ft_errno(PARSE_ERR, temp->content);
        ft_lstdel(node);
    }
}

static unsigned int	length_to_operater(char *str, unsigned int index,
					char rounds)
{
	unsigned int	length;
	t_parse			quotes;

	length = 0;
	quotes.s = 0;
	quotes.d = 0;

	while (str[index])
	{
		if (chk_slash(str, (int)index - 1) == 0)
		{
			chk_quotes(str[index], &quotes.s, &quotes.d, '\"');
			chk_quotes(str[index], &quotes.d, &quotes.s, '\'');
			if (quotes.s == 0 && quotes.d == 0 &&
				chk_operaters(str[index]) == rounds)
				return (length);
		}
		index++;
		length++;
	}

	return (length);
}

/*
******************* PUBLIC *****************************************************
*/

void				lexer(t_list **tokens, char *str)
{
	unsigned int	index;
	unsigned int	length;
	char			rounds;
	char			*word;
	t_list			*node;

	CHK_CV(str);

	index = 0;
	rounds = 1;
	node = NULL;

	while (str[index])
	{
		length = length_to_operater(str, index, rounds);
		rounds = rounds == 1 ? -1 : 1;
		word = ft_strsub(str, index, length);
		ft_addtolist(&node, word);
		ft_strdel(&word);
		index += length;
	}

	chk_lexer(&node);
	*tokens = node;
}
