/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_errno.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/02 13:35:14 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/03 19:34:58 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/sh.h"

const t_errno		g_err[] =
{
	(t_errno){PARSE_ERR, &parse_error},
	(t_errno){SYNTAX_ERR, &syntax_error},
	(t_errno){EXEC_ERR, &exec_error},
	(t_errno){FD_ERR, &fd_error},
	(t_errno){DIR_ERR, &dir_error},
	(t_errno){EXIT_ERR, &exit_error},
    (t_errno){CD_ERR, &cd_error},
    (t_errno){MLC_ERR, &malloc_error}
};

/*
******************* PRIVATE ****************************************************
*/

void				malloc_error(char *str __attribute__((unused)))
{
    ft_putendl_fd("Out of memory", 2);
    exit(EXIT_FAILURE);
}

void				cd_error(char *str)
{
    ft_putstr_fd("21sh: cd: ", 2);
    ft_putstr_fd(str, 2);
    ft_putendl_fd(" not set", 2);
}
void				exit_error(char *str)
{
	ft_putstr_fd("21sh: ", 2);
	ft_putstr_fd(str, 2);
	ft_putendl_fd(": Numeric argument required", 2);
}

void				dir_error(char *str)
{
	ft_putstr_fd("21sh: ", 2);
	ft_putstr_fd(str, 2);
	ft_putendl_fd(": Ambiguous redirect", 2);
	exit(EXIT_FAILURE);
}

void				fd_error(char *str)
{
	ft_putstr_fd("21sh: ", 2);
	ft_putstr_fd(str, 2);
	ft_putendl_fd(": Bad file descriptor", 2);
	exit(EXIT_FAILURE);
}

void				exec_error(char *str)
{
	ft_putstr_fd("21sh: command not found: ", 2);
	ft_putendl_fd(str, 2);
	exit(EXIT_FAILURE);
}

void				syntax_error(char *str __attribute__((unused)))
{
	ft_putendl_fd("21sh: syntax not respected", 2);
}

void				parse_error(char *str)
{
	ft_putstr_fd("21sh: parse error near: ", 2);
	ft_putendl_fd(str, 2);
}

/*
******************* PUBLIC *****************************************************
*/


char				ft_errno(int err, char *str)
{
	register unsigned short	index;

	index = 0;
	while (index < 7)
	{
		if (g_err[index].err == err)
			g_err[index].fct(str);
		index++;
	}
return (-1);
}
