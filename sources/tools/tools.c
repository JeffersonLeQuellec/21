/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/20 19:24:55 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/02 20:39:41 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh.h"

/*
******************* PRIVATE ****************************************************
*/

/*
******************* PUBLIC *****************************************************
*/

void 				ft_treedel(t_ast **node)
{
	CHK_CV(*node)
	ft_treedel(&(*node)->left);
	ft_treedel(&(*node)->right);
	ft_strdel(&(*node)->operater);
	ft_arraydel(&(*node)->command);
	free(*node);
	*node = NULL;
}

t_ast				*create_node(int place, char *operater, char **command)
{
	t_ast	*new;

	CHK_CC((new = (t_ast*)malloc(sizeof(t_ast))));
	new->place = place ? place : 0;
	new->operater = operater ? ft_strdup(operater) : NULL;
	new->command = command ? command : NULL;
	new->left = NULL;
	new->right = NULL;
	return (new);
}

int					add_to_tree(t_ast **node, int place, char *operater, char **command)
{
	if (*node == NULL)
	{
		CHK_CI((*node = create_node(place, operater, command)));
		return (0);
	}
	if (place < (*node)->place)
		return (add_to_tree(&(*node)->right, place, operater, command));
	else if (place >= (*node)->place)
		return (add_to_tree(&(*node)->left, place, operater, command));
	else
		return (0);
}

char				create_access(char **str, char **paths)
{
	register unsigned int	index;
	char					*path;

	index = 0;
	if (paths)
	{
		while (paths[index])
		{
			path = ft_threejoin(paths[index], "/", *str);
			if (access(path, X_OK) == 0)
			{
				ft_strdel(str);
				*str = ft_strdup(path);
				path ? ft_strdel(&path) : 0;
				return (0);
			}
			path ? ft_strdel(&path) : 0;
			index++;
		}
	}
	return (-1);
}
