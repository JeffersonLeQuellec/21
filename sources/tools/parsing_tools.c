/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing_tools.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/02 14:31:02 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/03 01:36:06 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh.h"

/*
******************* PRIVATE ****************************************************
*/

/*
******************* PUBLIC *****************************************************
*/

char				chk_operaters(char c)
{
	register unsigned short     index;
    static const char           operater[] =
	{'&', '|', '<', '>', ';', '\0'};

    index = 0;
    while (operater[index])
    {
        if (c == operater[index])
            return (1);
        index++;
    }
    return (-1);
}

char				chk_slash(char *str, int index)
{
	unsigned int	occurence;

	occurence = 0;
	while (str[index])
	{
		if (str[index] == '\\')
			occurence++;
		else
			break ;
		index--;
	}
	return (occurence % 2);
}

char				chk_quotes(char current, char *flag, char *value,
					char search)
{
	if (*flag == 0)
	{
		if (current == search)
		{
			if (*value == 0)
			{
				*value = 1;
				return (1);
			}
			if (*value == 1)
			{
				*value = 0;
				return (0);
			}
		}
	}
	return (-1);
}
