/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env_tools.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/02 14:14:29 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/02 15:19:19 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh.h"

/*
******************* PRIVATE ****************************************************
*/

/*
******************* PUBLIC *****************************************************
*/

int					find_env_variable(char **env, char *str)
{
	size_t			index;
	unsigned int	length;
	char			*new;

	index = 0;
	while (env[index])
	{
		length = ft_strrlen(env[index], '=', 0);
		new = ft_strsub(env[index], 0, length);
		if (ft_strcmp(str, new) == 0)
		{
			ft_strdel(& new);
			return (index);
		}
		ft_strdel(&new);
		index++;
	}
	return (-1);
}

char				*get_path_variable(char **env, char *var)
{
	int			place;
	char		*ret;

	ret = NULL;
	place = find_env_variable(env, var);
	if (place >= 0)
		ret = ft_strsub(env[place], ft_strrlen(env[place], '=', 0) + 1, ft_strlen(env[place]));
	return (ret);
}

char				**split_path_variable(char **env, char *var)
{
	char	*path;
	char	**paths;

	path = get_path_variable(env, var);
	paths = ft_strsplit(path, ':');
	ft_strdel(&path);
	return (paths);
}
