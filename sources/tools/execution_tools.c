/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   execution_tools.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/02 20:14:37 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/03 02:40:54 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh.h"

/*
******************* PRIVATE ****************************************************
*/

/*
******************* PUBLIC *****************************************************
*/

void                do_execution(t_ast *root, t_line *var,
					t_process *flag __attribute__((unused)))
{
    execve(*root->command, root->command, var->env);
	ft_errno(EXEC_ERR, *root->command);
}
