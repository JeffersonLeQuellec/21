/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   commands.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/23 16:46:39 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/03 20:27:47 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/sh.h"

/*
******************* PRIVATE ****************************************************
*/

static inline char	**do_expansion(t_list *node, char **env)
{
	char	**ret;
	char	**paths;

	ret = split(node->content);
	if (node->before == NULL || *node->before->content == ';' ||
	*node->before->content == '|')
	{
		paths = split_path_variable(env, "PATH");
		if (paths)
		{
            *ret != NULL ? create_access(&(*ret), paths) : 0;
			ft_arraydel(&paths);
		}
	}
	return (ret);
}

/*
******************* PUBLIC *****************************************************
*/

void				populate_commands(t_ast **root, t_list *node, char **env)
{
	register unsigned int	index;
	unsigned int			place;

	index = 0;
	place = 0;
	while (node)
	{
		if (index % 2 == 0)
		{
			add_to_tree(root, place, NULL, do_expansion(node, env));
			place++;
		}
		index++;
		node = node->before;
	}
}
