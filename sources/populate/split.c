/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   split.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/02 19:02:25 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/02 19:03:04 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/sh.h"

/*
******************* PRIVATE ****************************************************
*/

static unsigned int	word_number(char *str)
{
	register unsigned int	index;
	unsigned int			nb;
	char					flag;

	index = 0;
	nb = 1;
	flag = 0;

	while (str[index])
	{
		if (str[index] == '\"')
		 	flag = flag == 0 ? 1 : 0;
		if (str[index] == ' ' && flag == 0)
			nb++;
		index++;
	}
	return (nb);
}

static unsigned int	word_length(char *str, unsigned int index)
{
	char			flag;

	flag = 0;

	while (str[index])
	{
		if (str[index] == '\"')
		 	flag = flag == 0 ? 1 : 0;
		if (str[index] == ' ' && flag == 0)
			break ;
		index++;
	}
	return (index);
}

/*
******************* PUBLIC *****************************************************
*/

char				**split(char *str)
{
	unsigned int	index;
	unsigned int	start;
	unsigned int	i;
	char			**array;

	index = 0;
	i = 0;
	index = ft_skip(str, index, ' ');
	array = (char**)malloc(sizeof(char*) * word_number(str) + 1);
	while (str[index])
	{
		start = index;
		index = word_length(str, index);
		array[i++] = ft_strsub(str, start, index - start);
		ft_remove_char(&array[i - 1], '\"');
		index = ft_skip(str, index, ' ');
	}
	array[i] = NULL;
	return (array);
}
