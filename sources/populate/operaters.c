/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   operaters.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/07 11:06:56 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/02 19:03:31 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/sh.h"

/*
******************* PRIVATE ****************************************************
*/

/*
******************* PUBLIC *****************************************************
*/

void				captain(t_ast **root, unsigned int place, char *str,
					char c __attribute__((unused)))
{
	if (*str != ';' && *str != '|')
		add_to_tree(root, place, str, NULL);
}

void				colonel(t_ast **root, unsigned int place, char *str, char c)
{
	if (*str == c)
		add_to_tree(root, place, str, NULL);
}

void				populate_operaters(t_ast **root, t_list *node, char c,
	void(*f)(t_ast **root, unsigned int place, char *str, char c))
{
	register unsigned int	index;
	unsigned int			place;

	index = 0;
	place = 0;
	while (node)
	{
		if (index % 2 == 1)
		{
			place++;
			f(root, place, node->content, c);
		}
		node = node->before;
		index++;
	}
}
