/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/20 09:48:08 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/03 01:41:44 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/sh.h"

t_ast	    *g_root;
t_line	    *g_var;
t_process   *g_flag;

/*
******************* PRIVATE ****************************************************
*/

static inline void	terminate(t_ast **root, t_line *var)
{
	ft_treedel(root);
	ft_strdel(&var->line);
	ft_arraydel(&var->env);
	ft_lstdel(&var->tokens);
}

static inline void	initiate(t_ast **root, t_line *var, t_process *flag)
{
	extern char	**environ;

	*root = NULL;
	
	var->fd = open("/dev/tty", O_RDWR);
	var->line = NULL;
	var->tokens = NULL;
	var->env = ft_arraydup(environ);

    flag->builtins = 0;
    flag->forked = 0;
    flag->process = 0;
	flag->ret = 0;

	g_root = *root;
	g_var = var;
    g_flag = flag;

	ft_inittermcap();
}

/*
******************* PUBLIC *****************************************************
*/

int					main(void)
{
	t_ast	    *root;
	t_line	    var;
    t_process   flag;

	initiate(&root, &var, &flag);
	core(&root, &var, &flag);
	terminate(&root, &var);
    return (0);
}
