/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   core.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/20 16:10:40 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/03 19:52:17 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/sh.h"

/*
*************** PRIVATE ********************************************************
*/

static void		__attribute__((hot))
				initiate(t_line *var, char ret)
{
    print_prompt(var->fd, var->env, ret);
	get_next_line(0, &var->line);
}

static void		__attribute__((hot))
				parsing(t_line *var)
{
	lexer(&var->tokens, var->line);
	parsing_operaters(&var->tokens);
	parsing_words(&var->tokens);
	parsing_std(&var->tokens);
	parsing_redirection(&var->tokens);
}

static void		__attribute__((hot))
				expansion(t_line *var, unsigned char ret)
{
	tilde_expansion(&var->tokens, var->env);
    pid_expansion(&var->tokens, var->env);
    return_expansion(&var->tokens, ret);
    variable_expansion(&var->tokens, var->env);
	// backslash_expansion(&var->tokens);
	// quote_expansion(&var->tokens);
}

static void		__attribute__((hot))
				populate(t_ast **root, t_list *node, char **env)
{
    ft_pointers_next(&node, ft_lstlen(node));
    populate_operaters(root, node, ';', &colonel);
    populate_operaters(root, node, '|', &colonel);
    populate_operaters(root, node, 0, &captain);
    populate_commands(root, node, env);
}

static void		__attribute__((hot))
				delete(t_ast **root, t_line *var)
{
	ft_treedel(root);
	ft_strdel(&var->line);
	ft_lstdel(&var->tokens);
}

/*
*************** PUBLIC *********************************************************
*/

void			core(t_ast **root, t_line *var, t_process *flag)
{
	while (42)
	{
        initiate(var, flag->ret);
        parsing(var);
        expansion(var, flag->ret);
        populate(root, var->tokens, var->env);
        execution(*root, var, flag);
        delete(root, var);
	}
}
