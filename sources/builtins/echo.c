/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   echo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/28 13:36:20 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/03 20:27:45 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/sh.h"

/*
******************* PRIVATE ****************************************************
*/

/*
******************* PUBLIC *****************************************************
*/

unsigned char		echo_builtin(char **command,
					char ***env __attribute__((unused)),
					unsigned char ret __attribute__((unused)))
{
    register unsigned int   index;
    char                    nline;

    index = 1;
    nline = 1;
    if (command[1] && ft_strcmp(command[1], "-n") == 0)
    {
        nline = 0;
        index = 2;
    }
    while (command[index])
    {
        ft_putstr(command[index]);
        command[index + 1] ? ft_putchar(32) : 0;
        index++;
    }
    nline == 1 ? ft_putchar(10) : 0;
	return (0);
}
