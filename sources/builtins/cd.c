/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cd.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/28 13:37:33 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/03 17:38:54 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/sh.h"

/*
******************* PRIVATE ****************************************************
*/

static unsigned char    change_direction(char *direction, char **env)
{
    char    err;
    char    *home;

    if (direction)
    {
        err = chdir(direction);
    }
    else
    {
        home = get_path_variable(env, "HOME");
        if (home)
        {
            err = chdir(home);
        }
        else
        {
            ft_errno(CD_ERR, "HOME");
            return (-1);
        }
    }
    return (err);
}

/*
******************* PUBLIC *****************************************************
*/

unsigned char		    cd_builtin(char **command, char ***env,
                        unsigned char ret __attribute__((unused)))
{
    char    err;

    err = change_direction(command[1], *env);
	return (err);
}
