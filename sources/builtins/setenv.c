/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setenv.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/28 13:37:58 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/03 20:17:10 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/sh.h"

/*
******************* PRIVATE ****************************************************
*/

static void         modify_variable(char *str, int variable, char ***env)
{
    char    *new;
    char    *temp;

    temp = ft_strsub((*env)[variable], 0, ft_strrlen((*env)[variable], '=', 0));
    ft_strdel(&(*env)[variable]);

    new = ft_threejoin(temp, "=", str);
    (*env)[variable] = new;
    ft_strdel(&temp);
}

static void         add_variable(char **command, char ***env)
{
    register unsigned int   index;
    unsigned int            length;
    char                    **new;

    index = 0;
    length = ft_arraylen(*env);
    new = (char**)malloc(sizeof(char*) * length + 2);
    !new ? ft_errno(MLC_ERR, NULL) : 0;

    while (index < length)
    {
        new[index] = ft_strdup((*env)[index]);
        index++;
    }

    new[index] = ft_threejoin(command[1], "=", command[2]);
    new[index + 1] = NULL;

    ft_arraydel(env);
    *env = new;
}

/*
******************* PUBLIC *****************************************************
*/

unsigned char       setenv_builtin(char **command, char ***env,
                    unsigned char ret __attribute__((unused)))
{
    int     variable;

    if (command[1])
    {
        variable = find_env_variable(*env, command[1]);
        if (variable >= 0)
            modify_variable(command[2], variable, env);
        else
            add_variable(command, env);
    }
    else
        ft_printarray(*env);
    return (0);
}
