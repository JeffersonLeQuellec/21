/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/03 02:58:37 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/03 04:15:21 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh.h"

/*
******************* PRIVATE ****************************************************
*/

/*
******************* PUBLIC *****************************************************
*/

unsigned char		exit_builtin(char **command,
					char ***env __attribute__((unused)), unsigned char ret)
{
	if (command[1])
	{
		if (ft_strdigit(command[1]) == 1)
			exit((unsigned char)ft_atoi(command[1]));
		ft_errno(EXIT_ERR, command[1]);
	}
	exit(ret);
	return (0);
}
