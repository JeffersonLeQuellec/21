/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   branch.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/18 12:53:07 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/02 15:33:51 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/sh.h"
#include <stdio.h>

/*
******************* PRIVATE ****************************************************
*/

static unsigned int	get_length(void)
{
	int				fd;
	char			buf;
	unsigned int	length;

	fd = open("/tmp/.branch", O_RDONLY);
	length = 0;
	if (fd > 0)
	{
		read(fd, &buf, 1);
		if (buf != '*')
			return (length);
		read(fd, &buf, 1);
		while (read(fd, &buf, 1) > 0)
		{
			if (buf == '\n' || buf == ' ')
				break ;
			length++;
		}
	}
	close(fd);
	return (length);
}

static inline char	*get_line(void)
{
	register unsigned int	index;
	int						fd;
	unsigned int			length;
	char					*ret;
	char					buf;

	index = 0;
	fd = open("/tmp/.branch", O_RDONLY);
	length = get_length();
	ret = NULL;
	if (fd > 0 && length > 0)
	{
		ret = ft_memalloc(length + 1);
		read(fd, &buf, 2);
		while (read(fd, &buf, 1) > 0)
		{
			if (buf == '\n' || buf == ' ')
				break ;
			ret[index++] = buf;
		}
	}
	close(fd);
	return (ret);
}

static inline void	exec(char *command[], char **env)
{
	int		fd;
	int		err;
	pid_t	father;

	fd = open("/tmp/.branch", O_RDWR | O_CREAT | O_TRUNC, 0644);
	err = open("/dev/null", O_RDWR);

	father = fork();

	if (father == 0)
    {
		dup2(fd, STDOUT_FILENO);
		dup2(err, STDERR_FILENO);
		if (execve(command[0], command, env) == -1)
			exit(-1);
	}
	else
		wait(&father);

	close(fd);
	close(err);
}

/*
******************* PUBLIC *****************************************************
*/

char				*get_branch(char **env)
{
	static char		*command[] = {"/usr/bin/git", "branch", NULL};
	char			*branch;

	exec(command, env);
	branch = get_line();
	remove("/tmp/.branch");
	return (branch);
}
