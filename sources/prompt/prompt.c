/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prompt.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/20 12:24:23 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/03 02:39:14 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/sh.h"

/*
******************* PRIVATE ****************************************************
*/

static inline void	add_color(char **argv, char **env, char flag)
{
	static unsigned char	color = 255;
	char					*ret;
	char					*temp;
	char					*branch;

	branch = get_branch(env);
	ret = ft_memalloc(ft_strlen(*argv) + ft_strlen(branch) + 28);

	ft_strcpy(ret, "\e[38;5;");
	ft_strcat(ret, (temp = ft_itoa(color)));
	ft_strcat(ret, "m");
	ft_strcat(ret, *argv);

	if (branch)
	{
		ft_strcat(ret, " [");
		ft_strcat(ret, branch);
		ft_strcat(ret, "]");
		ft_strdel(&branch);
	}

	flag == 0 ? ft_strcat(ret, "\x1b[32m") : ft_strcat(ret, "\x1b[31m");
	ft_strcat(ret, " ❯ ");
	ft_strcat(ret, "\x1b[0m");

	ft_strdel(argv);
	ft_strdel(&temp);

	*argv = ret;
	color--;
	color == 233 ? color = 255 : 0;
}

static size_t	last_length(char *argv)
{
	size_t	index;
	size_t	length;

	index = 0;
	length = ft_strlen(argv) - 1;
	while (argv[length] != '/' && argv[length])
	{
		length--;
		index++;
	}
	return (index);
}

void			get_last(char **argv)
{
	size_t	length;
	size_t	index;
	char	*ret;

	length = last_length(*argv);
	index = ft_strlen(*argv) - length;
	ret = ft_strsub(*argv, index, length);
	ft_strdel(argv);
	*argv = ret;
}

/*
******************* PUBLIC *****************************************************
*/

void				print_prompt(int fd, char **env, char flag)
{
	char	*cwd;

	cwd = get_path_variable(env, "PWD");
	if (cwd)
	{
		get_last(&cwd);
		add_color(&cwd, env, flag);
		ft_putstr_fd(cwd, fd);
		ft_strdel(&cwd);
	}
	else
		ft_putstr_fd("\x1b[32m❯ \x1b[0m", fd);
}
