/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   right_redirection.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/23 14:07:48 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/03 03:12:37 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/sh.h"

/*
******************* PRIVATE ****************************************************
*/

static int			get_std(char *operater)
{
    unsigned int    index;
    unsigned int    direction;
    char            *new;

    index = 0;
    while (operater[index])
    {
        if (ft_isdigit(operater[index]) != 1)
            break ;
        index++;
    }
    if (index > 0)
    {
        new = ft_strsub(operater, 0, index);
        direction = ft_atoi(new);
        ft_strdel(&new);
        return (direction);
    }
    return (1);
}

static void          duplicate_fd(t_ast *root, unsigned int std, int fd)
{
    if (fd > 0)
    {
        if (ft_strchr(root->operater, '&'))
        {
            dup2(fd, STDOUT_FILENO);
            dup2(fd, STDERR_FILENO);
        }
        else
            dup2(fd, std);
    }
}

static void         open_fd(t_ast *root, unsigned int type, unsigned int std, int *fd)
{
    int     out;

    if (ft_strchr(root->operater, '&') && ft_strdigit(*root->right->command) == 1)
    {
        out = ft_atoi(*root->right->command);
        out > 2 ? ft_errno(FD_ERR, *(root->right->command)) : 0;
        std != 1 ? ft_errno(DIR_ERR, *(root->right->command)) : 0;
        *fd = open(*root->right->command, O_RDWR);
    }
    else
        *fd = open(*root->right->command, O_RDWR | O_CREAT | type, 0644);
}

/*
******************* PUBLIC *****************************************************
*/

void                right_redirection(t_ast *root, t_line *var, t_process *flag)
{
    unsigned int    std;
    unsigned int    length;
    int             type;
    int             fd;

    std = get_std(root->operater);
    length = ft_intlen(std);
    type = ft_strncmp(root->operater + length, ">>", 2) == 0 ? O_APPEND : O_TRUNC;

    open_fd(root, type, std, &fd);
    fd > 0 ? duplicate_fd(root, std, fd) : exit(-1);
    root->left->command ? do_execution(root->left, var, flag) : execution(root->left, var, flag);
    close(fd);
}
