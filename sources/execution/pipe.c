/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipe.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/02 20:42:55 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/02 21:44:21 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh.h"

/*
******************* PRIVATE ****************************************************
*/

static void			read_pipe(int *fd)
{
	close(fd[1]);
	dup2(fd[0], STDIN_FILENO);
	close(fd[0]);
}

static void			write_pipe(int *fd)
{
	close(fd[0]);
	dup2(fd[1], STDOUT_FILENO);
	close(fd[1]);
}

/*
******************* PUBLIC *****************************************************
*/

void                piipe(t_ast *root, t_line *var, t_process *flag)
{
	int		fd[2];
	pid_t	process;

	pipe(fd);
	process = fork();
	CHK_IV(process);

	if (process == 0)
	{
		read_pipe(fd);
		if (root->right->command)
			do_execution(root->right, var, flag);
		else
			execution(root->right, var, flag);
	}
	else
	{
		write_pipe(fd);
		if (root->left->command)
			do_execution(root->left, var, flag);
		else
			execution(root->left, var, flag);
	}
}
