/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   execution.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/18 08:57:11 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/03 20:27:26 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/sh.h"

/*
******************* PRIVATE ****************************************************
*/

static void         do_fork(t_ast *root, t_line *var, t_process *flag,
                    void(*f)(t_ast *root, t_line *var, t_process *flag))
{
    int		status;
    pid_t   process;

    process = fork();
    if (process != -1)
    {
        flag->forked = 1;
        if (process == 0)
            f(root, var, flag);
        else
        {
            wait(&status);
            flag->ret = WIFEXITED(status) ? WEXITSTATUS(status) : 0;
        }
    }
    flag->forked = 0;
}
//
/*
******************* PUBLIC *****************************************************
*/

void				execution(t_ast *root, t_line *var, t_process *flag)
{
    CHK_CV(root);
    if (root->operater)
    {
        // if (ft_strchr(root->operater, '>'))
        //     flag->forked == 0 ? do_fork(root, var, flag, &right_redirection)
		// 	: right_redirection(root, var, flag);
		// if (ft_strchr(root->operater, '|'))
		// 	flag->forked == 0 ? do_fork(root, var, flag, &piipe)
		// 	: piipe(root, var, flag);
    }
	else
	{
		if (builtins(root->command, &var->env, &flag->ret) == -1)
            do_fork(root, var, flag, &do_execution);
	}
}
