/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtins.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/03 02:43:29 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/03 20:27:23 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/sh.h"

const t_builtins	g_fct[] =
{
	(t_builtins){"/bin/echo", "echo", &echo_builtin},
	(t_builtins){"exit", NULL, &exit_builtin},
	(t_builtins){"/usr/bin/cd", "cd", &cd_builtin},
    (t_builtins){"setenv", NULL, &setenv_builtin}
};

/*
******************* PRIVATE ****************************************************
*/

/*
******************* PUBLIC *****************************************************
*/

char				builtins(char **command, char ***env, unsigned char *ret)
{
	unsigned short	index;

	index = 0;
	while (index < 4)
	{
		if (ft_strcmp(command[0], g_fct[index].s1) == 0 || ft_strcmp(command[0], g_fct[index].s2) == 0)
		{
			*ret = g_fct[index].fct(command, env, *ret);
			return (1);
		}
		index++;
	}
	return (-1);
}
