/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   variable_expansion.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/03 11:32:08 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/03 01:26:09 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh.h"
/*
******************* PRIVATE ****************************************************
*/

static void			do_expansion(char **str, char *rest, char *var, char *temp)
{
	char	*new;

	new = ft_memalloc(ft_strlen(*str) + ft_strlen(var));
	ft_strncpy(new, *str, ft_strlen(*str) - ft_strlen(rest));
	var ? ft_strcat(new, var) : 0;
	ft_strcat(new, rest + ft_strlen(temp) + 1);
	ft_strdel(str);
	*str = new;
}

static unsigned int	__attribute__((const))
					get_length(char *str)
{
	unsigned int	length;

	length = 0;
	while (str[length])
	{
		if (str[length] == ' ' || str[length] == '\"')
			break ;
		length++;
	}
	return (length);
}

static void			prepare_expansion(char **str, char **env,
					unsigned int index)
{
	char			*rest;
	char			*var;
	char			*word;

	rest = ft_strnchr(*str, '$', index);
	if (rest[1] && rest[1] != ' ')
	{
		word = ft_strsub(rest, 1, get_length(rest) - 1);
		var = get_path_variable(env, word);
		do_expansion(str, rest, var, word);
		ft_strdel(&word);
		ft_strdel(&var);
	}
}

static void			variable(char **str, char **env, t_parse *quotes)
{
	unsigned int	index;

	index = 0;
	while ((*str)[index])
	{
		if (chk_slash((*str), (int)index - 1) == 0)
		{
			chk_quotes((*str)[index], &quotes->s, &quotes->d, '\"');
			chk_quotes((*str)[index], &quotes->d, &quotes->s, '\'');
			if (quotes->s == 0 && (*str)[index] == '$')
				prepare_expansion(str, env, index);
		}
		index++;
	}
}

/*
******************* PUBLIC *****************************************************
*/

void				variable_expansion(t_list **node, char **env)
{
	register unsigned int	index;
	t_parse					quotes;
	t_list					*temp;

	index = 0;
	quotes.s = 0;
	quotes.d = 0;
	temp = *node;

	while (temp)
	{
		if (index % 2 == 0)
			variable(&temp->content, env, &quotes);
		index++;
		temp = temp->next;
	}
}
