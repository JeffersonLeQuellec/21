/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   return_expansion.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/03 17:42:14 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/03 18:49:41 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/sh.h"

/*
******************* PRIVATE ****************************************************
*/

static  void        do_expansion(char **str, char *rest, char *number)
{
    char    *new;

    new = ft_memalloc(ft_strlen(*str) + ft_strlen(number));
    ft_strncpy(new, *str, ft_strlen(*str) - ft_strlen(rest));
    ft_strcat(new, number);
    ft_strcat(new, rest + 2);
    ft_strdel(str);
    *str = new;
}

static void			prepare_expansion(char **str, unsigned int index,
                    unsigned char ret)
{
    char	*rest;
    char    *number;

    rest = ft_strnchr(*str, '$', index);
    if (ft_strncmp(rest, "$?", 2) == 0)
    {
        number = ft_itoa(ret);
        do_expansion(str, rest, number);
        ft_strdel(&number);
    }
}


static void			retuurn(char **str, t_parse *quotes,
                    unsigned char ret __attribute__((unused)))
{
	unsigned int	index;

	index = 0;
	while ((*str)[index])
	{
		if (chk_slash((*str), (int)index - 1) == 0)
		{
			chk_quotes((*str)[index], &quotes->s, &quotes->d, '\"');
			chk_quotes((*str)[index], &quotes->d, &quotes->s, '\'');
			if ((*str)[index] == '$')
                prepare_expansion(str, index, ret);
		}
		index++;
	}
}

/*
******************* PUBLIC *****************************************************
*/

void				return_expansion(t_list **node, unsigned char ret)
{
	register unsigned int	index;
	t_parse					quotes;
	t_list					*temp;

	index = 0;
	quotes.s = 0;
	quotes.d = 0;
	temp = *node;

	while (temp)
	{
		if (index % 2 == 0)
			retuurn(&temp->content, &quotes, ret);
		index++;
		temp = temp->next;
	}
}
