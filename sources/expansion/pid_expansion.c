/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pid_expansion.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/02 23:41:46 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/03 01:25:47 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh.h"

/*
******************* PRIVATE ****************************************************
*/

static void			do_expansion(char **str, char *rest, char *pid)
{
	char	*new;

	new = ft_memalloc(ft_strlen(*str) + ft_strlen(pid));
	ft_strncpy(new, *str, ft_strlen(*str) - ft_strlen(rest));
	ft_strcat(new, pid);
	ft_strcat(new, rest + 2);
	ft_strdel(str);
	*str = new;
}

static void			prepare_expansion(char **str, unsigned int index)
{
	char	*rest;
	char	*pid;

	rest = ft_strnchr(*str, '$', index);
	if (ft_strncmp(rest, "$$", 2) == 0)
	{
		pid = ft_strdup(ft_itoa(getpid()));
		do_expansion(str, rest, pid);
		ft_strdel(&pid);
	}
}

static void			pid(char **str, t_parse *quotes)
{
	unsigned int	index;

	index = 0;
	while ((*str)[index])
	{
		if (chk_slash(*str, (int)index - 1) == 0)
		{
			chk_quotes((*str)[index], &quotes->s, &quotes->d, '\"');
			chk_quotes((*str)[index], &quotes->d, &quotes->s, '\'');
			if (quotes->s == 0 && (*str)[index] == '$')
				prepare_expansion(str, index);
		}
		index++;
	}
}

/*
******************* PUBLIC *****************************************************
*/

void				pid_expansion(t_list **node,
					char **env __attribute__((unused)))
{
	register unsigned int	index;
	t_parse					quotes;
	t_list					*temp;

	index = 0;
	quotes.s = 0;
	quotes.d = 0;
	temp = *node;

	while (temp)
	{
		if (index % 2 == 0)
			pid(&temp->content, &quotes);
		index++;
		temp = temp->next;
	}
}
