/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   quote_expansion.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/05 12:00:39 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/03 01:37:26 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/sh.h"

// /*
// ******************* PRIVATE ****************************************************
// */
//
// static inline void	do_expansion(char **str, unsigned int *index)
// {
// 	char	*rest;
// 	char	*new;
//
// 	rest = ft_strchr(*str, '\'');
// 	new = ft_memalloc(ft_strlen(*str));
// 	ft_strncpy(new, *str, ft_strlen(*str) - ft_strlen(rest));
// 	ft_strcat(new, rest + 1);
// 	ft_strdel(str);
// 	*str = new;
// 	*index -= 1;
// }
//
// static void			quote(char **str, t_parse *quotes)
// {
// 	unsigned int	index;
//
// 	index = 0;
// 	while ((*str)[index])
// 	{
// 	    if (chk_slash(*str, (int)index - 1) == 0)
// 	    {
// 	        chk_quotes((*str)[index], &quotes->s, &quotes->d, '\"');
// 	        chk_quotes((*str)[index], &quotes->d, &quotes->s, '\'');
// 	        if (quotes->d == 0 && (*str)[index] == '\'')
// 				do_expansion(str, &index);
// 	    }
// 	    index++;
// 	}
// }
//
// /*
// ******************* PUBLIC *****************************************************
// */
//
// void				quote_expansion(t_list **node)
// {
// 	register unsigned int	index;
// 	t_parse					quotes;
// 	t_list					*temp;
//
// 	index = 0;
// 	quotes.s = 0;
// 	quotes.d = 0;
// 	temp = *node;
//
// 	while (temp)
// 	{
// 		if (index % 2 == 0)
// 			quote(&temp->content, &quotes);
// 		index++;
// 		temp = temp->next;
// 	}
// 	index++;
// }
