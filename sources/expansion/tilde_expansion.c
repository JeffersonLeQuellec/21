/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tilde_expansion.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/02 16:30:32 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/03 01:25:27 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh.h"

/*
******************* PRIVATE ****************************************************
*/

static void			do_expansion(char **str, char *rest, char *var)
{
	char			*new;

	new = ft_memalloc(ft_strlen(*str) + ft_strlen(var));
	ft_strncpy(new, *str, ft_strrlen(*str, '~', 0));
	ft_strcat(new, var);
	ft_strcat(new, rest + 1);
	ft_strdel(str);
	*str = new;
}

static void			prepare_expansion(char **str, char **env)
{
	char	*rest;
	char	*var;

	rest = ft_strchr(*str, '~');
	var = get_path_variable(env, "HOME");
	rest && var ? do_expansion(str, rest, var) : 0;
	ft_strdel(&var);
}

static char			chk(char *str, unsigned int index)
{
	if (index == 0 || !str[index - 1] || str[index - 1] == ' ')
	{
		if (!str[index + 1] || str[index + 1] == ' ')
		{
			if (str[index] == '~')
				return (1);
		}
	}
	return (-1);
}

static void			tilde(char **str, char **env, t_parse *quotes)
{
	unsigned int	index;

	index = 0;
	while ((*str)[index])
	{
		if (chk_slash((*str), (int)index - 1) == 0)
		{
			chk_quotes((*str)[index], &quotes->s, &quotes->d, '\"');
			chk_quotes((*str)[index], &quotes->d, &quotes->s, '\'');
			if (quotes->s == 0 && quotes->d == 0 && chk((*str), index) == 1)
				prepare_expansion(str, env);
		}
		index++;
	}
}

/*
******************* PUBLIC *****************************************************
*/


void				tilde_expansion(t_list **node, char **env)
{
	register unsigned int	index;
	t_parse					quotes;
	t_list					*temp;

	index = 0;
	quotes.s = 0;
	quotes.d = 0;
	temp = *node;

	while (temp)
	{
		if (index % 2 == 0)
			tilde(&temp->content, env, &quotes);
		index++;
		temp = temp->next;
	}
}
