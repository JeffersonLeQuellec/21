# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/06/02 12:01:32 by jle-quel          #+#    #+#              #
#    Updated: 2017/10/03 19:03:32 by jle-quel         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = 21sh

FLAGS += -Wall -Wextra -Werror -O2 -I Includes

SRC =	sources/core/core.c						\
		sources/core/main.c						\
												\
		sources/prompt/branch.c					\
		sources/prompt/prompt.c					\
												\
		sources/tools/env_tools.c				\
		sources/tools/ft_errno.c				\
		sources/tools/parsing_tools.c			\
		sources/tools/tools.c					\
		sources/tools/execution_tools.c			\
												\
		sources/parsing/lexer.c					\
		sources/parsing/parsing_operaters.c		\
		sources/parsing/parsing_words.c			\
		sources/parsing/parsing_std.c			\
		sources/parsing/parsing_redirection.c	\
												\
		sources/expansion/tilde_expansion.c		\
		sources/expansion/variable_expansion.c	\
		sources/expansion/pid_expansion.c		\
		sources/expansion/quote_expansion.c		\
		sources/expansion/backslash_expansion.c	\
		sources/expansion/return_expansion.c	\
												\
		sources/populate/operaters.c			\
		sources/populate/commands.c				\
		sources/populate/split.c				\
												\
		sources/execution/execution.c			\
		sources/execution/right_redirection.c	\
		sources/execution/pipe.c				\
		sources/execution/builtins.c			\
												\
		sources/builtins/echo.c					\
		sources/builtins/exit.c					\
		sources/builtins/cd.c					\
		sources/builtins/setenv.c				\

OBJ = $(SRC:.c=.o)

LIBFT = libft/libft.a

.PHONY = all clean fclean clean re

all: $(NAME)

$(OBJ): %.o: %.c
	@gcc -c $(FLAGS) $< -o $@

$(LIBFT):
	@make -C libft

$(NAME): $(LIBFT) $(OBJ)
	@gcc $(OBJ) $(LIBFT) -ltermcap -o $(NAME)
	@echo "\033[32mCompiled Executable From 21sh\033[0m"

fs:
	gcc $(FLAGS) -g3 -fsanitize=address -L libft -lft $(OBJ) -ltermcap -o $(NAME)

clean:
	@rm -rf $(OBJ)
	@make -C libft clean
	@echo "\033[32mRemoved Object Files From 21sh\033[0m"

fclean: clean
	@rm -rf $(NAME)
	@make -C libft fclean
	@echo "\033[32mRemoved Executable From 21sh\033[0m"

re: fclean
	@make
