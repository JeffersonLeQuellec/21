/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sh.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jle-quel <jle-quel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/16 13:57:20 by jle-quel          #+#    #+#             */
/*   Updated: 2017/10/03 20:27:29 by jle-quel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SH_H
# define SH_H

/*
********************** LIBRARY *************************************************
*/

# include "../libft/includes/libft.h"
# include <fcntl.h>
# include <sys/stat.h>
# include <sys/ioctl.h>
# include <signal.h>

/*
********************** DEFINES *************************************************
*/

# define PATH_MAX 4096

# define PARSE_ERR 666
# define SYNTAX_ERR 667
# define EXEC_ERR 668
# define FD_ERR 669
# define DIR_ERR 670
# define EXIT_ERR 671
# define CD_ERR 672
# define MLC_ERR 673

/*
********************** STRUCTURES **********************************************
*/

typedef struct		s_ast
{
	int				place;
	char			*operater;
	char			**command;
	struct s_ast	*left;
	struct s_ast	*right;
}					t_ast;

typedef struct		s_parse
{
	char			s;
	char			d;
}					t_parse;

typedef struct		s_line
{
	int				fd;
	char			*line;
	char			**env;
	t_list			*tokens;
	t_list			*clipboard;
}					t_line;

typedef struct      s_process
{
    short           builtins;
    char            forked;
    pid_t           process;
	unsigned char	ret;
}                   t_process;

typedef struct		s_errno
{
	int				err;
	void			(*fct)(char *str);
}					t_errno;

typedef struct		s_function
{
	char			*str;
	void			(*fct)(t_ast *root, t_line *var, t_process *flag);
}					t_function;

typedef struct		s_builtins
{
	char			*s1;
    char            *s2;
	unsigned char	(*fct)(char **str, char ***env, unsigned char ret);
}					t_builtins;

/*
******************* GLOBALS ****************************************************
*/

extern t_ast		*g_root;
extern t_line		*g_var;
extern t_process    *g_flag;

/*
******************* CORE *******************************************************
*/

void				core(t_ast **root, t_line *var, t_process *flag);

/*
******************* PROMPT *****************************************************
*/

void				print_prompt(int fd, char **env, char flag);
char				*get_branch(char **env);

/*
******************* ENV ********************************************************
*/

char				*get_path_variable(char **env, char *var);
char				**split_path_variable(char **env, char *envVariable);
int					find_env_variable(char **env, char *str);

/*
******************* PARSING ****************************************************
*/

void				lexer(t_list **tokens, char *str);
void				parsing_operaters(t_list **node);
void				parsing_words(t_list **node);
void                parsing_std(t_list **node);
void                parsing_redirection(t_list **node);

char				chk_operaters(char c);
char				chk_slash(char *str, int index);
char				chk_quotes(char current, char *flag, char *value,
					char search);

/*
******************* ERRNO ******************************************************
*/

char				ft_errno(int err, char *str);
void				parse_error(char *str);
void				syntax_error(char *str);
void				exec_error(char *str);
void				fd_error(char *str);
void				dir_error(char *str);
void				exit_error(char *str);
void				cd_error(char *str);
void				malloc_error(char *str);


/*
******************* EXPANSION **************************************************
*/

void				tilde_expansion(t_list **node, char **env);
void				variable_expansion(t_list **node, char **env);
void				pid_expansion(t_list **node, char **env);
void				quote_expansion(t_list **node);
void				backslash_expansion(t_list **node);
void                return_expansion(t_list **node, unsigned char ret);

/*
******************* TOOLS ******************************************************
*/

int					add_to_tree(t_ast **node, int place, char *operater,
					char **command);
t_ast				*create_node(int place, char *operater, char **command);
void 				ft_treedel(t_ast **node);
char				create_access(char **str, char **paths);


/*
******************* POPULATE ***************************************************
*/

void				captain(t_ast **root, unsigned int place, char *str,
					char c __attribute__((unused)));
void				colonel(t_ast **root, unsigned int place, char *str,
					char c);
void				populate_operaters(t_ast **root, t_list *node, char c,
					void(*f)(t_ast **root, unsigned int place, char *str,
					char c));
void				populate_commands(t_ast **root, t_list *node, char **env);
char				**split(char *str);

/*
******************* EXECUTION **************************************************
*/

void				execution(t_ast *root, t_line *var, t_process *flag);
void                right_redirection(t_ast *root, t_line *var, t_process *flag);
char				builtins(char **command, char ***env, unsigned char *ret);
void                piipe(t_ast *root, t_line *var, t_process *flag);

void                do_execution(t_ast *root, t_line *var, t_process *flag);

/*
******************* BUILTINS ***************************************************
*/

unsigned char		echo_builtin(char **command, char ***env, unsigned char ret);
unsigned char		exit_builtin(char **command, char ***env, unsigned char ret);
unsigned char		cd_builtin(char **command, char ***env, unsigned char ret);
unsigned char		setenv_builtin(char **command, char ***env, unsigned char ret);

#endif
